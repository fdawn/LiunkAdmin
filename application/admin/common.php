<?php
/**
 * Created by 流年酷.
 * User: WenZhen Miao
 * Date: 2018-04-10
 * Time: 下午 5:52
 */
/**
 * 判断是否登陆
 * author WenZhen Miao(admin@liunk.cn)
 */
function is_login()
{
	$user = session('user_auth');
	if (empty($user)) {
		return 0;
	} else {
		return session('user_auth_sign') == data_auth_sign($user) ? $user['uid'] : 0;
	}
}
//判断登录是否失效
function is_login_token(){

	 $login_token=Db::name('admin')->where('id',is_login())->value('login_token');

     if(session('user_auth.login_token')!==$login_token){
           return false;
     }
     //dump(session('user_auth.login_token'));
     return true;
}