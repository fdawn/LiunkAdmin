<?php
/**
 * Created by 流年酷.
 * User: WenZhen Miao
 * Date: 2018-04-18
 * Time: 下午 4:20
 */

namespace app\admin\controller;

use think\Db;
class System extends Base
{
		//角色设置
       public function group(){
	       return view('', ['title' => '角色设置']);
       }
       /**
		 * @param  string  $name 角色列表
		 * @return mixed
		 * @route('group/list','get')
		 */
		public function group_list(){
			if($this->request->isGet()){
				if (input('?get.key')) {
					$where['title'] = ['title', 'like', '%' . input('get.key') . '%'];
				} else {
					$where = '';
				}

				$list = Db('admin_group')->where($where)->page(input('get.page'), input('get.limit'))->select();
				$count = Db('admin_group')->where($where)->count('id');
				$data=[
					'code'=>0,
					'msg'=>'查询成功',
					'count'=>$count,
					'data'=>$list,
				];
				return $data;
			}
		}

	/**
	 * @param  string $name 添加角色
	 * @return mixed
	 * @route('group/add')
	 */
	public function group_add()
	{
		if ($this->request->isGet()) {
			return view('', ['title' => '添加角色']);
		} else {
			$input = input();
			if (!$input['title']) {
				$data = [
					'code' => 200,
					'msg' => '标题不能为空',
				];
				return $data;
			}
			$where = ['title' => $input['title']];
			$title = db('admin_group')->where($where)->find();
			if ($title) {
				$data = [
					'code' => 200,
					'msg' => '标题不能重复',
				];
				return $data;
			}
			$data = ['title' => $input['title'], 'status' => $input['status'], 'rules' => implode(',', $input['treeGirdCheckbox']), 'describe' => $input['describe']];
			$show = db('admin_group')->data($data)->insert();
			if ($show) {
				$data = [
					'code' => 0,
					'msg' => '添加成功',
				];
			} else {
				$data = [
					'code' => 200,
					'msg' => '添加失败',
				];
			}
			return $data;
		}
	}

		/**
		 * @param  string  $name 编辑角色
		 * @return mixed
		 * @route('group/edit')
		 */
		public function group_edit(){
			if($this->request->isGet()){
				$where=array('id'=>input('get.id'));
				$info=db('admin_group')->where($where)->find();
				return view('', ['title' => '编辑角色','lnk_info'=>$info]);
			}else{
				$input=input();
				if(!$input['title']){
					$data=[
						'code'=>200,
						'msg'=>'标题不能为空',
					];
					return $data;
				}
				$where = ['title' => $input['title']];
				$title = db('admin_group')->where($where)->find();
				if ($title) {
					$data = [
						'code' => 200,
						'msg' => '标题不能重复',
					];
					return $data;
				}
				$show=db('admin_group')->update(['title' => $input['title'],'status'=>$input['status'],'rules'=>implode(',',$input['treeGirdCheckbox']),'describe'=>$input['describe'],'id'=>$input['id']]);
                if($show){
	                $data=[
		                'code'=>0,
		                'msg'=>'编辑成功',
	                ];
                }else{
	                $data=[
		                'code'=>200,
		                'msg'=>'编辑失败',
	                ];
                }
				return $data;
			}
		}
	/**
	 * @param  string  $name 角色权限
	 * @return mixed
	 * @route('group/rule','post')
	 */
	public function group_rule(){
		if($this->request->isPost()){
             $id=input('post.id');
             $where=array('status'=>1);
			 $where_group=array('id'=>$id);
			 $info=db('admin_group')->where($where_group)->value('rules');
			 $info_id = explode(",",$info);
             $list=db('admin_menu')->where($where)->select();
             $list=list_to_tree($list,'id','pid','children');
            //dump( $where_group);
			 $data=[];
             foreach($list as $k=>$v){
                 $data[$k]['id']=$v['id'];
                 $data[$k]['name']=$v['title'];
                 if(in_array($v['id'],$info_id)){
                 	$data[$k]['checked']=true;
                 }
                 if(isset($v['children'])){
		             $data[$k]['children']=[];
	                 foreach($v['children'] as $key=>$value){
	                     $data[$k]['children'][$key]['id']=$value['id'];
		                 $data[$k]['children'][$key]['name']=$value['title'];
		                 if(in_array($value['id'],$info_id)){
			                 $data[$k]['children'][$key]['checked']=true;
		                 }
	                 }
                 }
             }
             return $data;
		}
	}

	/**
	 * @param  string $name 编辑角色状态
	 * @return mixed
	 * @route('group/usable','post')
	 */
	public function usable_status()
	{
		if ($this->request->isPost()) {
			$id = input('post.id');
			$status = input('post.statue');
			$where = ['id' => $id];
			$data['status'] = $status == 1 ? 0 : 1;
			if (db('admin_group')->where($where)->setField($data)) {
				$data = [
					'code' => 0,
					'msg' => '编辑成功',
				];
				return $data;
			} else {
				$data = [
					'code' => 200,
					'msg' => '编辑失败',
				];
				return $data;
			}

		}
	}
}