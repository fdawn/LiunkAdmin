<?php
namespace app\admin\controller;

use think\Db;

class Index extends Base
{
	/**
	 * @param  string  $name 后台首页
	 * @return mixed
	 * @route('index/index','get')
	 */
    public function index()
    {
    	$where=array('pid'=>0,'status'=>1);
    	$where_group=array('uid'=>is_login());
    	$group_id=db('admin_group_access')->where($where_group)->value('group_id');
    	$where_rule=array('id'=>$group_id,'status'=>1);
    	$rules=db('admin_group')->where($where_rule)->field('rules,title')->find();

    	session('userRights',$rules['title']);
	    $rules=explode(",",$rules['rules']);
    	$menu_top=db('admin_menu')->where($where)->select();
    	$top_menu=[];
    	foreach($menu_top as $k=>$v){
		    if (in_array($v['id'],$rules)){
			    $top_menu[$k]=$v;
			    $er_menu[$k]['href']=url($v['href']);
		    }
	    }
	    return view('', ['title' => '后台首页','admin_menu'=>$top_menu]);
    }
	/**
	 * @param  string  $name 后台主页
	 * @return mixed
	 * @route('index/main','get')
	 */
	public function main()
	{

		return view('', ['title' => '后台首页']);
	}
	/**
	 * @param  string  $name 返回menujson数据
	 * @return mixed
	 * @route('index/menu_json','get')
	 */
	public function menu(){
         if($this->request->isGet()){
	         $where=array('pid'=>input('get.id'),'status'=>1);
	        // $menu=list_to_tree(session('auth_menu_'.is_login()),'id','pid');
	         $where_group=array('uid'=>is_login());
	         $group_id=db('admin_group_access')->where($where_group)->value('group_id');
	         $where_rule=array('id'=>$group_id,'status'=>1);
	         $rules=db('admin_group')->where($where_rule)->value('rules');
	         $rules=explode(",",$rules);
	         $menu_er=db('admin_menu')->where($where)->select();
	         $er_menu=[];
	         foreach($menu_er as $k=>$v){
		         if (in_array($v['id'],$rules)){
			         $er_menu[$k]=$v;
			         $er_menu[$k]['href']=url($v['href']);
		         }
	         }
	         return $er_menu;
         }
	}
	/**
	 * @param  string  $name 后台主页
	 * @return mixed
	 * @route('index/config_ajax','post')
	 */
	public  function main_ajax(){
		if($this->request->isPost()){
             $data=[
             	"cmsName"=>config('app_name'),
				"version"=> config('app_version'),
				"author"=> think_decrypt("MDAwMDAwMDAwMJaardKWq3lraHnKaQ",'20170129'),
				"homePage"=> $_SERVER["SERVER_SOFTWARE"],
				"server"=> PHP_OS,
				"dataBase"=>$this->_mysql_version(),
				"maxUpload"=> ini_get('upload_max_filesize'),
				"userRights"=>session('userRights'),
				"description"=> "流年酷的后台管理cms",
				"powerby"=> "copyright @".date('Y',time())." LIUNKCMS",
				"record"=>"陇ICP备14040xxx号-1",
				"keywords"=>""
             ];
             return $data;
		}
	}
	/**
	 * @param  string  $name 用户数量
	 * @return mixed
	 * @route('index/user_count_ajax','get')
	 */
	public function main_user_count(){
		if($this->request->isGet()) {
			$data = [
				'count' => 20,
			];
			return $data;
		}
	}
	/**
	 * @param  string  $name 友链数量
	 * @return mixed
	 * @route('index/link_ajax','get')
	 */
	public function main_link_count(){
		if($this->request->isGet()) {
			$data = [
				'count' => 10,
			];
			return $data;
		}
	}
	private function _mysql_version()
	{
		new Db();
		$version = Db::query("select version() as ver");
		return $version[0]['ver'];
	}

}
